# SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
# SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
# SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
#
# SPDX-License-Identifier: GPL-2.0-only

QT       += core gui widgets network
TARGET = smuGUI

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    main.cpp \
    smugui.cpp

HEADERS += \
    smugui.h

include(../smuLIB/smuLIB.pri)

FORMS += \
    smugui.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
