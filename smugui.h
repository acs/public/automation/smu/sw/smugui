// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#ifndef SMUGUI_H
#define SMUGUI_H

#include <subcfg.h>
#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>

namespace Ui {
class smuGUI;
}

class QTcpSocket;

class smuGUI : public QMainWindow
{
    Q_OBJECT
    subCFG *smuCFG;
    bool connected;

public:
    explicit smuGUI(QWidget *parent = nullptr);
    ~smuGUI();
    
protected:
    void paintEvent(QPaintEvent *e) override;

private slots:
    void read_tcp();
    void write_tcp(QString msg);
    void update_log();
    void update_gui();
    void update_cfg();

    void on_bt_connect_clicked();
    void on_bt_reset_clicked();
    void on_bt_push_clicked();
    void on_bt_fetch_clicked();
    void on_bt_load_clicked();
    void on_bt_save_clicked();
    void on_bt_selectfile_clicked();

private:
    Ui::smuGUI *ui;
    QTcpSocket *mSocket;

    QString conf,buffer,file_name;
    QFile *file;
    QMessageBox message;
};

#endif // SMUGUI_H
