// SPDX-FileCopyrightText: 2017-2021 Carlo Guarnieri <carlo.guarnieri@ieee.org>
// SPDX-FileCopyrightText: 2019-2021 César Andrés Cazal <cesar.cazal@eonerc.rwth-aachen.de>
// SPDX-FileCopyrightText: 2021 Institute for Automation of Complex Power Systems, EONERC
//
// SPDX-License-Identifier: GPL-2.0-only

#include "smugui.h"
#include "ui_smugui.h"
#include <QTcpSocket>
#include <QPainter>

smuGUI::smuGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::smuGUI)
{
    ui->setupUi(this);
    this->setFixedSize(QSize(600, 420));

    connected = false;

    mSocket = new QTcpSocket(this);
    connect(mSocket,&QTcpSocket::readyRead,this,&smuGUI::read_tcp);
    connect(mSocket,&QTcpSocket::connected,[&](){
        connected = true;
        ui->label_connection->setText("Connected");
        ui->bt_connect->setText("Disconnect");
        ui->bt_push->setEnabled(1);
        ui->bt_fetch->setEnabled(1);
        ui->bt_reset->setEnabled(1);
        update();
    });
    connect(mSocket,&QTcpSocket::disconnected,[&](){
        connected = false;
        ui->label_connection->setText("Disconnected");
        ui->bt_connect->setText("Connect");
        ui->bt_push->setEnabled(0);
        ui->bt_fetch->setEnabled(0);
        ui->bt_reset->setEnabled(0);
        update();
    });

    file_name = SMU_CFG;
    smuCFG = new subCFG();
    QObject::connect(smuCFG, SIGNAL(updated()), this, SLOT(update_gui()));

    QObject::connect(ui->cbox_daq_rate,SIGNAL(currentIndexChanged(int)),this,SLOT(update_cfg()));
    QObject::connect(ui->cbox_dsp_rate,SIGNAL(currentIndexChanged(int)),this,SLOT(update_cfg()));
    QObject::connect(ui->ck_channel1,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->ck_channel2,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->ck_channel3,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->ck_channel4,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->ck_channel5,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->ck_channel6,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->ck_channel7,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->ck_channel8,SIGNAL(stateChanged(int)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel1,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel2,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel3,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel4,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel5,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel6,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel7,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->lb_channel8,SIGNAL(textChanged(QString)), this, SLOT(update_cfg()));
    QObject::connect(ui->ob_mode_freerun,SIGNAL(clicked()), this, SLOT(update_cfg()));
    QObject::connect(ui->ob_mode_oneshot,SIGNAL(clicked()), this, SLOT(update_cfg()));
    QObject::connect(ui->ob_sync_none,SIGNAL(clicked()), this, SLOT(update_cfg()));
    QObject::connect(ui->ob_sync_pps,SIGNAL(clicked()), this, SLOT(update_cfg()));

    update_gui();
}

smuGUI::~smuGUI()
{
    delete ui;
}

// Draw connection led indicator
void smuGUI::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    if (connected)
        painter.setBrush(Qt::green);
    else
        painter.setBrush(Qt::red);
    painter.drawEllipse(455,357,20,20);
    QWidget::paintEvent(e);
}

// Read the message via TCP
void smuGUI::read_tcp(){
    QTextStream chunk(mSocket);

    buffer = buffer + chunk.readAll();
    int i;

    if (buffer.contains('{')){
        while ((buffer.count('}')) >= 1){
            conf = buffer.section('}',0,0);
            if ((i = conf.count('{'))>0){
                conf = conf.section('{',i,i);
                update_log();
                update_gui();
            }
            if (buffer.count('{')>1){
                if (i>0)
                    buffer.remove(buffer.indexOf('{'),buffer.indexOf('}')-buffer.indexOf('{')+1);
                else
                    buffer.remove(0,buffer.indexOf('}')+1);
            }
            else
                buffer.clear();
        }
    }
    else {
        buffer.clear();
    }
}

// Send the message via TCP
void smuGUI::write_tcp(QString msg){
    QTextStream Text(mSocket);
    if( mSocket->waitForConnected() ) {
        Text << msg;
        mSocket->flush();
    }
}

// Update log status
void smuGUI::update_log(){
    smuCFG->toString(conf,';');
    ui->list_current_config->clear();
    ui->list_current_config->addItems(conf.split(';'));
}

// Update GUI from config
void smuGUI::update_gui(){

    ui->cbox_daq_rate->setCurrentText(QString::number(smuCFG->daq.rate));
    ui->cbox_dsp_rate->setCurrentText(QString::number(smuCFG->dsp.rate));

    ui->lb_channel1->setText(smuCFG->dsp.alias[0]);
    ui->lb_channel2->setText(smuCFG->dsp.alias[1]);
    ui->lb_channel3->setText(smuCFG->dsp.alias[2]);
    ui->lb_channel4->setText(smuCFG->dsp.alias[3]);
    ui->lb_channel5->setText(smuCFG->dsp.alias[4]);
    ui->lb_channel6->setText(smuCFG->dsp.alias[5]);
    ui->lb_channel7->setText(smuCFG->dsp.alias[6]);
    ui->lb_channel8->setText(smuCFG->dsp.alias[7]);

    ui->ck_channel1->setChecked(smuCFG->dsp.state[0].toInt());
    ui->ck_channel2->setChecked(smuCFG->dsp.state[1].toInt());
    ui->ck_channel3->setChecked(smuCFG->dsp.state[2].toInt());
    ui->ck_channel4->setChecked(smuCFG->dsp.state[3].toInt());
    ui->ck_channel5->setChecked(smuCFG->dsp.state[4].toInt());
    ui->ck_channel6->setChecked(smuCFG->dsp.state[5].toInt());
    ui->ck_channel7->setChecked(smuCFG->dsp.state[6].toInt());
    ui->ck_channel8->setChecked(smuCFG->dsp.state[7].toInt());

    if (smuCFG->daq.mode == MODE_FREERUN)
        ui->ob_mode_freerun->setChecked(1);
    else
        ui->ob_mode_oneshot->setChecked(1);

    if (smuCFG->daq.sync == SYNC_NONE)
        ui->ob_sync_none->setChecked(1);
    else if (smuCFG->daq.sync == SYNC_PPS)
        ui->ob_sync_pps->setChecked(1);
    else
        ui->ob_sync_ntp->setChecked(1);
}

// Update GUI from config
void smuGUI::update_cfg(){

    smuCFG->daq.rate = (daq_rate_t)ui->cbox_daq_rate->currentText().toInt();
    smuCFG->dsp.rate = (dsp_rate_t)ui->cbox_dsp_rate->currentText().toInt();

    smuCFG->dsp.alias[0] = ui->lb_channel1->text();
    smuCFG->dsp.alias[1] = ui->lb_channel2->text();
    smuCFG->dsp.alias[2] = ui->lb_channel3->text();
    smuCFG->dsp.alias[3] = ui->lb_channel4->text();
    smuCFG->dsp.alias[4] = ui->lb_channel5->text();
    smuCFG->dsp.alias[5] = ui->lb_channel6->text();
    smuCFG->dsp.alias[6] = ui->lb_channel7->text();
    smuCFG->dsp.alias[7] = ui->lb_channel8->text();

    smuCFG->dsp.state[0] = ui->ck_channel1->isChecked() ? "1" : "0";
    smuCFG->dsp.state[1] = ui->ck_channel2->isChecked() ? "1" : "0";
    smuCFG->dsp.state[2] = ui->ck_channel3->isChecked() ? "1" : "0";
    smuCFG->dsp.state[3] = ui->ck_channel4->isChecked() ? "1" : "0";
    smuCFG->dsp.state[4] = ui->ck_channel5->isChecked() ? "1" : "0";
    smuCFG->dsp.state[5] = ui->ck_channel6->isChecked() ? "1" : "0";
    smuCFG->dsp.state[6] = ui->ck_channel7->isChecked() ? "1" : "0";
    smuCFG->dsp.state[7] = ui->ck_channel8->isChecked() ? "1" : "0";

    smuCFG->daq.mode = ui->ob_mode_freerun->isChecked() ? MODE_FREERUN : MODE_ONESHOT;
    smuCFG->daq.sync = ui->ob_sync_pps->isChecked() ? SYNC_PPS : SYNC_NONE;

    update_log();
}

// ------------------------  EVENTS  -------------------------

// Connect/Disconnect to/from SMU host
void smuGUI::on_bt_connect_clicked(){
    if (connected){
        mSocket->disconnectFromHost();
    } else {
        mSocket->connectToHost(ui->lb_server_name->text(),ui->lb_port_value->text().toUShort());
        ui->label_connection->setText("Connecting...");
    }
}

// Reset SMU host
void smuGUI::on_bt_reset_clicked(){
    write_tcp("[Reset]");
}

// Push configuration to SMU
void smuGUI::on_bt_push_clicked(){
    smuCFG->toString(conf,';');
    write_tcp('{' + conf + '}');
    write_tcp("[Conf]");
}

// Fetch configuration from SMU
void smuGUI::on_bt_fetch_clicked(){
    write_tcp("[Conf]");
}

// Load SMU config file
void smuGUI::on_bt_load_clicked(){
    smuCFG->load();
    message.setText("Config file loaded");
    message.exec();
    update_log();
    update_gui();
}

// Save SMU config file
void smuGUI::on_bt_save_clicked(){
    smuCFG->save();
    message.setText("Config file saved");
    message.exec();
    update_log();
}

// Select SMU configuration file
void smuGUI::on_bt_selectfile_clicked(){
    file_name = QFileDialog::getOpenFileName(this,"Select config file");
    smuCFG->file = new QFile(file_name);
    ui->lb_file->setText(file_name);
}
