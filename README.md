# **smuGUI** <br/> _Graphical user interface for remote SMU configuration_

smuGUI is simple GUI tool designed for remote SMU configuration. It allows to start/stop and configure the service to change the smuDSP and smuNET modules at runtime.

[<img src="docs/smuGUI.png"  width="944" height="500">](docs/smuGUI.png)

## Copyright
2017-2021, Carlo Guarnieri (ACS) <br/>
2019-2021, César Andrés Cazal (ACS) <br/>
2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="LICENSE"><img alt="Apache License v2.0" style="border-width:0" src="docs/apache-2_logo.png" width="88"/></a><br />This work is licensed under the <a rel="license" href="LICENSE">Apache License v2.0</a>.

## Funding
<a rel="funding" href="https://hyperride.eu/"><img alt="HYPERRIDE" style="border-width:0" src="docs/hyperride_logo.png" height="63"/></a>&nbsp;
<a rel="funding" href="https://cordis.europa.eu/project/id/957788"><img alt="H2020" style="border-width:0" src="https://hyperride.eu/wp-content/uploads/2020/10/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Hyperride" href="https://hyperride.eu/">HYbrid Provision of Energy based on Reliability and Resiliency via Integration of DC Equipment</a> (HYPERRIDE), a project funded by the European Union's Horizon 2020 research and innovation programme under <a rel="H2020" href="https://cordis.europa.eu/project/id/957788"> grant agreement No. 957788</a>.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [César Andrés Cazal](mailto:cesar.cazal@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
